import { INestApplication } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { PORT } from './config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  
  await docsBootstrap(app);
  await app.listen(PORT);
}

async function docsBootstrap(app: INestApplication) {
  const config = new DocumentBuilder()
    .setTitle('Nest APIs')
    .setDescription('APIs Documentation for Redis JSON and Redis Search')
    .setVersion('0.01')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);
}
bootstrap();
