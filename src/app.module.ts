import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ItemModule } from './redis/item/item.module';
import { RedisManagerModule } from './redis/redis.module';

@Module({
  imports: [RedisManagerModule, ItemModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
