import { load } from 'ts-dotenv';

const config = load({
  PORT: Number,

  REDIS_URL: String,
  REDIS_PORT: Number,
  REDIS_DATABASE_INDEX: Number,
});

export const PORT = config.PORT;
export const REDIS_URL = config.REDIS_URL;
export const REDIS_PORT = config.REDIS_PORT;
export const REDIS_DATABASE_INDEX = config.REDIS_DATABASE_INDEX;

