import { Logger } from "@nestjs/common";
import { createClient } from "redis";
import { REDIS_DATABASE_INDEX, REDIS_PORT, REDIS_URL } from "src/config";
import { ECustomRedis, RedisService } from "../redis.type";

const logger = new Logger('RedisMainProvider');

export const RedisMainProvider = {
  provide: ECustomRedis.MAIN,
  useFactory: async (): Promise<RedisService> => {
    try {

      const client: RedisService = await createClient({
        url: `redis://${REDIS_URL}:${REDIS_PORT}`,
        database: REDIS_DATABASE_INDEX,
      });

      await client.connect();
      
      logger.log(`connect-url: ${REDIS_URL}:${REDIS_PORT}`);

      return client;
    } catch (error) {
      throw error;
    }
  },
};
