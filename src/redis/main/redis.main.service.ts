import { Inject, Injectable, Logger } from '@nestjs/common';
import { RedisJSON } from '@redis/json/dist/commands';
import { RediSearchSchema, SearchOptions } from 'redis';
import { ECustomRedis, RedisService } from '../redis.type';

const logger = new Logger('RedisMainService');

@Injectable()
export class RedisMainService {
  constructor(
    @Inject(ECustomRedis.MAIN) 
    private redisMainService: RedisService,

  ) { }

  public async getData<T>(key: string): Promise<T | string | null> {
    try {
      const response = await this.redisMainService.get(key);
      if (!response) return null;

      try {
        return JSON.parse(response);
      } catch (error) {
        return response;
      }
    } catch (err) {
      logger.error(err.message, err.stack);
    }
  }

  public async setData(
    key: string,
    value: string,
    ttl: number = null,
  ): Promise<void> {
    try {
      //TODO:implement prefix service-name
      if (ttl) { //TODO: improve multi
        await this.redisMainService.set(key, value);
        await this.redisMainService.expire(key, ttl);
      } else {
        await this.redisMainService.set(key, value);
      }
    } catch (err) {
      logger.error(err.message, err.stack);
    }
  }

  public async hashSetData(key: string, value: Array<[string, string]>) {
    try {
      await this.redisMainService.HSET(key, value);
    } catch (e) {
      logger.error(e.message, e.stack);
      throw e;
    }
  }

  public async hashGetData(
    key: string,
    field: string | string[],
  ): Promise<string | string[]> {
    try {
      if (typeof field == 'string') {
        const result = await this.redisMainService.HGET(key, field);
        return result;
      }
      const result = await this.redisMainService.HMGET(key, field);
      return result;
    } catch (e) {
      logger.error(e.message, e.stack);
      throw e;
    }
  }

  public async hashDeleteData(key: string, field: string | string[]) {
    try {
      const result = await this.redisMainService.HDEL(key, field);
      return result;
    } catch (e) {
      logger.error(e.message, e.stack);
      throw e;
    }
  }

  //#region JSON Path
  public async JSONGetData(key: string) {
    try {
      const result = await this.redisMainService.json.GET(key);
    } catch (e) {
      logger.error(e.message, e.stack);
      throw e;
    }
  }
  //#endregion

  //#region RedisSearch
  public async searchData(index: string, query: string, option: SearchOptions) {
    try {
      const result = await this.redisMainService.ft.search(
        index,
        query,
        option,
      );
      return result;
    } catch (e) {
      logger.error(e.message, e.stack);
      throw e;
    }
  }

  public async createIndex(
    index: string,
    prefixDoc: string,
    schema: RediSearchSchema,
  ) {
    try {
      const result = await this.redisMainService.ft.CREATE(index, schema, {
        ON: 'JSON',
        PREFIX: prefixDoc,
      });
      return result;
    } catch (e) {
      logger.error(e.message, e.stack);
      throw e;
    }
  }

  public async addJSONData(key: string, data: RedisJSON, path = '$') {
    try {
      const result = await this.redisMainService.json.SET(key, path, data);
      return result;
    } catch (e) {
      logger.error(e.message, e.stack);
      throw e;
    }
  }

  public async deleteJSONData(key: string, pathForDelete?: string) {
    try {
      const result = await this.redisMainService.json.DEL(key, pathForDelete);
      return result;
    } catch (e) {
      logger.error(e.message, e.stack);
      throw e;
    }
  }

  //#endregion
}
