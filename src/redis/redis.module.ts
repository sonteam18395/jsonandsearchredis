import { Module } from '@nestjs/common';
import { RedisManagerController } from './redis.controller';
import { RedisMainProvider } from './main/redis.main';
import { RedisMainService } from './main/redis.main.service';

@Module({
    controllers: [RedisManagerController],
    providers: [
        RedisMainProvider,
        RedisMainService
    ],
    exports: [
        RedisMainProvider,
        RedisMainService
    ],
})
export class RedisManagerModule { }
