import { Injectable, Logger, OnModuleInit } from "@nestjs/common";
import { RedisMainService } from "../main/redis.main.service";
import { IndexItemName, ItemSchema, PrefixItem, TItemRedisJSON } from "./item.type";

const logger = new Logger('RewardContractService');

@Injectable()
export class ItemService implements OnModuleInit {

    constructor(
        private redisMainService: RedisMainService,
    ) {

    }

    /**
     * Interface defining method called once the host module has been initialized.
     */
    async onModuleInit() {
        await this.createIndexSearch();
    }
    
    //#region Main
    public async createIndexSearch() {
        const response = await this.redisMainService.createIndex(IndexItemName, PrefixItem, ItemSchema);
        return response;
    }

    public async searchJsonData() {
        const responseData = await this.redisMainService.searchData(IndexItemName, '@price:[-inf +inf]', {
            LIMIT: { from: 0, size: 10 },
        });
        return responseData;
    }

    public async createJsonData() {
        const testData: TItemRedisJSON = {
            owner: '0x1234 ',
            name: 'item-001',
            price: 10,
            attribute: {
                hp: 12,
                atk: 17,
                def: 20
            }
        }
        await this.redisMainService.addJSONData(`${PrefixItem}:test-01`, testData);
        return { status: 'Add Success' };
    }

    public async deleteJsonData() {
        await this.redisMainService.deleteJSONData(`${PrefixItem}:test-01`);
        return { status: 'Delete Success' }
    }
    //#endregion
}