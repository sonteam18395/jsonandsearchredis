import { Module } from '@nestjs/common';
import { RedisManagerModule } from '../redis.module';
import { ItemController } from './item.controller';
import { ItemService } from './item.service';

@Module({
    imports: [RedisManagerModule],
    controllers: [ItemController],
    providers: [ItemService],
})
export class ItemModule { }
