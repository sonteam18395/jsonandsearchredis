import { Controller, Delete, Get, Put } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ItemService } from './item.service';

@ApiTags('item-main')
@Controller('item')
export class ItemController {
  constructor(private itemService: ItemService) { }

  @Put('/main/create-index')
  async mainCreateIndex(){
    const result = await this.itemService.createIndexSearch();
    return result;
  }

  @Get('/main/get-data')
  async mainGetData() {
    const result = await this.itemService.searchJsonData();
    return result;
  }

  @Put('/main/create-data')
  async mainCreateData(){
    const result = await this.itemService.createJsonData();
    return result;
  }

  @Delete('/main/delete-data')
  async mainDeleteData(){
    const result = await this.itemService.deleteJsonData();
    return result;
  }
}
