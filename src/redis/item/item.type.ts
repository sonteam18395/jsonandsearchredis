import { RediSearchSchema, SchemaFieldTypes } from '@redis/search';
import { RedisJSON } from '@redis/json/dist/commands';



/**
 * Schema Data For Create Index For use RedisSearch
 */
export const ItemSchema: RediSearchSchema = {
    '$.owner': { type: SchemaFieldTypes.TAG, AS: 'owner' },
    '$.name': { type: SchemaFieldTypes.TEXT, AS: 'name', SORTABLE: true },
    '$.price': { type: SchemaFieldTypes.NUMERIC, AS: 'price', SORTABLE: true },
    '$.atrribute.hp': { type: SchemaFieldTypes.NUMERIC, AS: 'hp', SORTABLE: true },
    '$.atrribute.atk': { type: SchemaFieldTypes.NUMERIC, AS: 'atk', SORTABLE: true },
    '$.atrribute.def': { type: SchemaFieldTypes.NUMERIC, AS: 'def', SORTABLE: true },
}
export const PrefixItem = 'ItemDoc';
export const IndexItemName = 'Item:Index';

export type TItemRedisJSON = IItemData & RedisJSON;
export interface IItemData {
    owner: string;
    name: string;
    price: number;
    attribute: {
        hp: number;
        atk: number;
        def: number;
    }
}