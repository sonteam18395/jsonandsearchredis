import { RedisClientType, RedisDefaultModules } from 'redis';

export type RedisService = RedisClientType<RedisDefaultModules>;

export enum ECustomRedis {
    MAIN = 'REDIS_MAIN',
}