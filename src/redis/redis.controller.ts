import { Controller, Delete, Get, Put } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RedisMainService } from './main/redis.main.service';

@ApiTags('redis')
@Controller('redis')
export class RedisManagerController {
  constructor(private redisService: RedisMainService) { }

  @Get('testGetData')
  async testGetData() {
    const responseData = await this.redisService.getData('testSetData');
    console.log('getData - responseData', responseData);

    return responseData;
  }

  @Get('testSetData')
  async testSetData() {
    await this.redisService.setData('testSetData', 'hello-testdata', 10);
    return { status: 'setData Success' };
  }
}
